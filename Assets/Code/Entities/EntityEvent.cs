﻿//
// Copyright (c) 2018 Jason Bricco
//

public enum EntityEvent
{
	Update,
	RoomChanged,
	Kill,
	HealthChanged,
	Count
}
