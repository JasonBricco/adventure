﻿//
// Copyright (c) 2018 Jason Bricco
//

public enum TileType
{
	Air,
	DungeonWall,
	DungeonWall1,
	DungeonWall2,
	DungeonWall3,
	DungeonWall4,
	DungeonWall5,
	DungeonWall6,
	DungeonWall7,
	Barrier,
	Portal,
	DungeonFloor,
	Spikes,
	Torch,
	PlainsGrass,
	PlainsWall,
	PlainsWall1,
	PlainsWall2,
	PlainsWall3,
	PlainsWall4,
	PlainsWall5,
	PlainsWall6,
	PlainsWall7,
	PlainsDoor,
	Count
}
