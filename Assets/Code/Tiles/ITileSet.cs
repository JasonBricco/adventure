﻿//
// Copyright (c) 2018 Jason Bricco
//

public interface ITileSet
{
	void OnSet(Vec2i tPos, TileComponent tc);
}
